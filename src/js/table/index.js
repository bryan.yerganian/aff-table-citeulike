const table = () => {
  Array.from(
    document.getElementsByClassName("affiliate-table__item_stars"),
    (element) => {
      // add tag b to span
      const starsCount = parseFloat(
        element.querySelector("span").innerHTML.match(/.+(?=\/)/i)[0]
      );

      element.querySelector("span").innerHTML = element
        .querySelector("span")
        .innerHTML.replace(starsCount, "<b>$&</b>");

      // clone star svgs
      const starsWrapper = element.querySelector("div");

      for (let i = 1; i < starsCount; i++) {
        const starSvg = element.querySelector("svg").cloneNode(true);
        starSvg.id = `star${i}`;
        starsWrapper.appendChild(starSvg);
      }

      //move starblock in another block

      if (window.innerWidth < 768) {
        const starBlock = document.createElement("div");
        starBlock.setAttribute("class", element.classList[0]);
        starBlock.innerHTML = element.innerHTML;

        element.parentElement
          .querySelector(".affiliate-table__item_meta div")
          .insertBefore(
            starBlock,
            element.parentElement.querySelector(
              ".affiliate-table__item_meta div a"
            )
          );

        element.remove();
      }
    }
  );

  Array.from(document.querySelectorAll('.affiliate-table__item'), (element, index) => {
    element.querySelector('.affiliate-table__item_meta span').innerHTML = index < 10 ? '0' + ++index : index;
  })
};

export default table();
